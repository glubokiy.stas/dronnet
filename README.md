# DronNet


## Installation

Install Pipenv globally:
```
pip install pipenv
```

Install dependencies:
```
pipenv install --three
```

Install PostgreSQL:
```
sudo apt-get install python-pip python-dev libpq-dev postgresql postgresql-contrib
```

Install PostGIS:
```
sudo apt-get install -y postgis postgresql-9.*-postgis-2.*
```


## Setting database

Create user and database:
```
sudo -u postgres createuser -P USER_NAME_HERE
sudo -u postgres createdb -O USER_NAME_HERE dronnet
```

Add PostGIS support to the database:
```
sudo -u postgres psql -c "CREATE EXTENSION postgis; CREATE EXTENSION postgis_topology;" dronnet
```


## Run application using Pipenv

Set environment variables:
```
export DB_USER=USER_NAME_HERE
export DB_PASSWORD=PASSWORD_HERE
export DB_HOST=DB_HOST_HERE
export API_URL=API_URL_HERE
```

Activate the virtual environment:
```
pipenv shell
```

Run Django development server:
```
python manage.py runserver
```


## Run application using Docker

File `.env` contains a list of the environment variables, which are required for the successful build. It can be modified by using following instructions:
```
POSTGRES_USER=USER_NAME_HERE
POSTGRES_PASSWORD=PASSWORD_HERE

DJANGO_SETTINGS_MODULE=dronnet.settings.local
DB_USER=USER_NAME_EQUAL_TO_POSTGRES_USER_HERE
DB_PASSWORD=PASSWORD_EQUAL_TO_POSTGRES_PASSWORD_HERE
DB_HOST=postgis

PORT=PORT_TO_EXPOSE_HERE
API_URL=API_URL_WITH_PORT_HERE
```

Run following console command after successful installation of Docker and Docker Compose:
```
docker-compose up
```

Run following console command in order to rebuild DronNet container (for rebuilding static or installing new dependencies):
```
docker-compose up --build
```
