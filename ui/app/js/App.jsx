import React from 'react';
import ReactDOM from 'react-dom';
import './style.global.scss';
import {Route} from "react-router-dom";
import {connect, Provider} from 'react-redux';
import {uiOpenSection, uiToggleLegend} from './redux-modules/ui/actions';
import MainSectionContainer from './components/layout/MainSectionContainer';
import HeaderContainer from './components/layout/HeaderContainer';
import 'babel-polyfill';
import {ConnectedRouter} from 'react-router-redux';
import ReservationsMapContainer from "./components/layout/ReservationsMapContainer";
import configureStore from './configureStore';
import createHistory from 'history/createBrowserHistory';
import Legend from "./components/layout/Legend/Legend";
import {fetchUserAsync} from './redux-modules/auth/actions';
import './services/enums';
import * as enumService from './services/enums';
import * as authService from './services/auth';


@connect(
    (state) => ({
        isLegendOpen: state.ui.isLegendOpen
    }),
    {uiOpenSection, fetchUserAsync, uiToggleLegend}
)
class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {userFetched: false};
    }

    componentWillMount() {
        const {history} = this.props;
        this.unsubscribeFromHistory = history.listen(this.handleLocationChange);
        this.handleLocationChange(history.location);

        if (localStorage.getItem('token')) {
            this.props.fetchUserAsync().then(
                () => this.setState({userFetched: true}),
                () => {
                    authService.logOut();
                    window.location.href = '/';
                }
            );
        } else {
            this.setState({userFetched: true});
        }
    }

    componentWillUnmount() {
        if (this.unsubscribeFromHistory) this.unsubscribeFromHistory();
    }

    handleLocationChange = () => {
        this.props.uiOpenSection();
    };

    render() {
        if (!this.state.userFetched) {
            return null;
        }

        return (
            <div>
                <HeaderContainer/>
                <MainSectionContainer/>
                <Legend onLegendClick={this.props.uiToggleLegend} isOpen={this.props.isLegendOpen}/>
                <ReservationsMapContainer/>
            </div>
        );
    }
}


const history = createHistory();
export const store = configureStore(history);

enumService.fetchEnums().then(() =>
    ReactDOM.render((
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <Route component={App}/>
            </ConnectedRouter>
        </Provider>
    ), document.getElementById('app-container'))
);
