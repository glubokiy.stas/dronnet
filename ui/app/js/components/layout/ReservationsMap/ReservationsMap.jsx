import React from 'react';
import {Circle, Polygon} from 'react-google-maps';
import {InfoBox} from 'react-google-maps/lib/components/addons/InfoBox';
import {DrawingManager} from 'react-google-maps/lib/components/drawing/DrawingManager';
import GoogleMaps from "../../utils/GoogleMaps/GoogleMaps";
import {MAP} from 'react-google-maps/lib/constants';
import PropTypes from 'prop-types';
import {getCoordinatesFromPolygon} from '../../../utils/utils';
import {CIRCLE, NODRAW, POLYGON} from "../../../redux-modules/draw/index";


export default class ReservationsMap extends React.Component {
    static propTypes = {
        defaultCenter: PropTypes.object,
        defaultZoom: PropTypes.number,
        drawFillColor: PropTypes.string,
        drawStrokeColor: PropTypes.string,
        drawMode: PropTypes.number,
        drawnCircle: PropTypes.object,
        drawnPolygon: PropTypes.object,
        mapType: PropTypes.string,
        onBoundsChange: PropTypes.func,
        onCircleCenterChange: PropTypes.func,
        onCircleRadiusChange: PropTypes.func,
        onPolygonComplete: PropTypes.func,
        reservations: PropTypes.array.isRequired,
    };

    static defaultProps = {
        defaultCenter: {lat: 0, lng: 0},
        defaultZoom: 2,
        drawFillColor: 'black',
        drawStrokeColor: 'black',
        drawMode: NODRAW,
        drawnCircle: {},
        drawnPolygon: {},
        mapType: 'roadmap',
        onBoundsChange: () => {},
        onCircleCenterChange: () => {},
        onCircleRadiusChange: () => {},
        onPolygonComplete: () => {},
    };

    fitBounds(bounds) {
        this.mapRef.fitBounds(bounds);
    }

    fitBoundsAndZoomOut(bounds, zoomOffset = 1) {
        this.fitBounds(bounds);
        this.mapRef.setZoom(this.mapRef.getZoom() - zoomOffset);
    }

    setCenter(center) {
        this.mapRef.setCenter(center);
    }

    setZoom(zoom) {
        this.mapRef.setZoom(zoom);
    }

    setMapType(type) {
        this.mapRef.setMapTypeId(type);
    }

    handleCircleDragEnd = () => {
        const center = this.circle.getCenter();
        this.props.onCircleCenterChange({lat: center.lat(), lng: center.lng()});
    };

    handleCircleRadiusChange = () => {
        this.props.onCircleRadiusChange(Math.round(this.circle.getRadius()));
    };

    handleMapClick = (event) => {
        if (this.props.drawMode === CIRCLE) {
            this.props.onCircleCenterChange({lat: event.latLng.lat(), lng: event.latLng.lng()});
        }
    };

    handleMapIdle = () => {
        // First onBoundsChange callback is silenced.
        if (this.isHandlingIdle) {
            this.props.onBoundsChange(this.mapRef);
        } else {
            this.isHandlingIdle = true;
        }
    };

    handleMapMounted = (c) => {
        if (!c || this.mapRef) {
            return;
        }

        this.mapRef = c.context[MAP];
        this.setMapType(this.props.mapType);
    };

    handlePolygonComplete = (e) => {
        // Remove drawing manager.
        e.setMap(null);

        this.props.onPolygonComplete(getCoordinatesFromPolygon(e));
    };

    handlePolygonDragEnd = () => {
        this.props.onPolygonComplete(getCoordinatesFromPolygon(this.polygon));
    };

    handlePolygonEdit = () => {
        this.props.onPolygonComplete(getCoordinatesFromPolygon(this.polygon));
    };

    render() {
        return (
            <GoogleMaps
                    defaultZoom={this.props.defaultZoom}
                    defaultCenter={this.props.defaultCenter}
                    onClick={this.handleMapClick}
                    onIdle={this.handleMapIdle}
                    onMapMounted={this.handleMapMounted}
                    clickableIcons={false}
                    options={{
                        mapTypeControl: true,
                        streetViewControl: false,
                    }}>

                {this.props.reservations.map((reservation) =>
                     <Polygon
                        path={reservation.coordinates}
                        key={reservation.id}
                        onClick={this.handleMapClick}
                        options={{
                            fillColor: reservation.fillColor,
                            strokeColor: reservation.strokeColor,
                        }}
                    />
                )}

                {this.props.reservations.map((reservation) =>
                    <InfoBox
                        defaultPosition={new google.maps.LatLng(reservation.coordinates[0])}
                        options={{closeBoxURL: ``, enableEventPropagation: true, pixelOffset: new google.maps.Size(-10, -5)}}
                        key={reservation.id}
                    >
                        <div style={{backgroundColor: reservation.strokeColor, opacity: 0.8, padding: `2px`, borderRadius: `3px`}}>
                            <div style={{fontSize: `11px`, color: `white`}}>
                                #{reservation.id}
                            </div>
                        </div>
                    </InfoBox>
                )}

                {this.props.drawMode === CIRCLE && this.props.drawnCircle.center.lat && this.props.drawnCircle.radius &&
                    <Circle
                        editable={true}
                        draggable={true}
                        onClick={this.handleMapClick}
                        onDragEnd={this.handleCircleDragEnd}
                        onRadiusChanged={this.handleCircleRadiusChange}
                        options={{fillColor: this.props.drawFillColor, strokeColor: this.props.drawStrokeColor}}
                        ref={(circle) => {this.circle = circle;}}
                        {...this.props.drawnCircle}
                    />
                }

                {this.props.drawMode === POLYGON &&
                    (!this.props.drawnPolygon.coordinates[0] ?
                        <DrawingManager
                            defaultDrawingMode={google.maps.drawing.OverlayType.POLYGON}
                            onPolygonComplete={this.handlePolygonComplete}
                            options={{
                                polygonOptions: {
                                    fillColor: this.props.drawFillColor,
                                    strokeColor: this.props.drawStrokeColor,
                                }
                            }}
                        />
                        :
                        <Polygon
                            editable={true}
                            draggable={true}
                            onDragEnd={this.handlePolygonDragEnd}
                            onMouseUp={this.handlePolygonEdit}
                            options={{fillColor: this.props.drawFillColor, strokeColor: this.props.drawStrokeColor}}
                            path={this.props.drawnPolygon.coordinates}
                            ref={(polygon) => {this.polygon = polygon;}}
                        />
                    )
                }
            </GoogleMaps>
        );
    }
}
