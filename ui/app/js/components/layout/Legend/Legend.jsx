import React from 'react';
import cn from 'classnames';
import s from './style.scss';
import PropTypes from 'prop-types';


export default class Legend extends React.Component {
    static propTypes = {
        isOpen: PropTypes.bool.isRequired,
        onLegendClick: PropTypes.func.isRequired,
    };

    render() {
        return (
            <div className={cn(s.legend, {[s.hidden]: !this.props.isOpen})}
                 onClick={this.props.onLegendClick}>
                <h3>Legend</h3>
                {
                    enums.reservationHeights.choices.map((choice) =>
                        <div key={choice.value}>
                            <span className={s.square} style={{
                                backgroundColor: enums.reservationHeights.fillColors[choice.value],
                                borderColor: enums.reservationHeights.strokeColors[choice.value]
                            }}> </span>
                            <span className={cn(s.legendText)}> {choice.name}</span>
                        </div>
                    )
                }
            </div>
        );
    }
}
