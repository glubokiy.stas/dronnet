import {connect} from 'react-redux';
import React from "react";
import ReservationsMap from "./ReservationsMap/ReservationsMap";
import {refreshReservationsByBoundsAsync} from "../../redux-modules/reservation/actions";
import {drawSetCenter, drawSetCoordinates, drawSetRadius} from "../../redux-modules/draw/actions";
import {saveMapPositionToLocalStorage} from "../../services/map";


@connect(
    (state) => ({
        bounds: state.map.bounds,
        center: state.map.center,
        defaultCenter: state.map.defaultCenter,
        defaultZoom: state.map.defaultZoom,
        drawFillColor: enums.reservationHeights.fillColors[state.newReservation.height],
        drawStrokeColor: enums.reservationHeights.strokeColors[state.newReservation.height],
        drawMode: state.draw.drawMode,
        drawnCircle: state.draw.circle,
        drawnPolygon: state.draw.polygon,
        mapType: state.map.mapType,
        reservations: state.reservation.items,
        zoomObj: state.map.zoomObj,
    }),
    {
        refreshReservationsByBoundsAsync,
        onCircleCenterChange: drawSetCenter,
        onCircleRadiusChange: drawSetRadius,
        onPolygonComplete: drawSetCoordinates,
    }
)
export default class ReservationsMapContainer extends React.Component {
    componentWillReceiveProps(nextProps) {
        if (nextProps.center !== this.props.center) {
            this.map.setCenter(nextProps.center);
        }

        if (nextProps.zoomObj !== this.props.zoomObj) {
            this.map.setZoom(nextProps.zoomObj.value);
        }

        if (nextProps.bounds !== this.props.bounds) {
            this.map.fitBoundsAndZoomOut(nextProps.bounds);
        }

        if (nextProps.mapType !== this.props.mapType) {
            this.map.setMapType(nextProps.mapType);
        }
    }

    handleBoundsChange = (map) => {
        const bounds = map.getBounds().toJSON();
        this.props.refreshReservationsByBoundsAsync(bounds);

        saveMapPositionToLocalStorage(map.getCenter().toJSON(), map.getZoom(), bounds);
    };

    render() {
        return (
            <ReservationsMap
                {...this.props}
                onBoundsChange={this.handleBoundsChange}
                ref={(map) => {this.map = map;}}
            />
        );
    }
}
