import React from 'react';
import AccountForm from "./AccountForm/AccountForm";
import {
    accountResetForm,
    accountSetAddress, accountSetFirstName, accountSetLastName,
    accountSetPhone, accountSetTimezone, setAccountForm, updateAccountAsync
} from "../../redux-modules/account/actions";
import {userReceive} from "../../redux-modules/auth/actions";
import {connect} from "react-redux";


@connect(
    (state) => ({
        firstName: state.account.firstName,
        lastName: state.account.lastName,
        address: state.account.address,
        phone: state.account.phone,
        timezone: state.account.timezone,
        isFetching: state.account.isFetching,
        errorMessage: state.account.errorMessage,
        successMessage: state.account.successMessage,
    }),
    {
        accountResetForm,
        setAccountForm,
        updateAccountAsync,
        userReceive,
        onFirstNameChange: accountSetFirstName,
        onLastNameChange: accountSetLastName,
        onAddressChange: accountSetAddress,
        onPhoneChange: accountSetPhone,
        onTimezoneChange: accountSetTimezone,
    }
)
export default class AccountFormContainer extends React.Component {
    componentDidMount() {
        this.props.setAccountForm();
    }

    componentWillUnmount() {
        this.props.accountResetForm();
    }

    handleFormSubmit = () => {
        this.props.updateAccountAsync().then(
            (response) => this.props.userReceive(response.data)
        );
    };

    render() {
        return (
            <AccountForm
                {...this.props}
                onFormSubmit={this.handleFormSubmit}
            />
        );
    }
}
