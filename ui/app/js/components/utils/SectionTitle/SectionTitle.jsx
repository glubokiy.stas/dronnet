import React from 'react';
import PropTypes from 'prop-types';
import Spinner from "../Spinner/Spinner";


export default class SectionTitle extends React.Component {
    static propTypes = {
        size: PropTypes.string,
        text: PropTypes.string.isRequired,
        withSpinner: PropTypes.bool,
    };

    static defaultProps = {
        size: '4',
        withSpinner: false,
    };

    render() {
        return (
            <div className="level">
                <div className="level-left">
                    <span className={`is-size-${this.props.size}`}>
                        {this.props.text}
                    </span>
                    {this.props.withSpinner &&
                        <Spinner small className="small-margin-left"/>
                    }
                </div>
                {this.props.children &&
                    <div className="level-right">
                        {this.props.children}
                    </div>
                }
            </div>
        );
    }
}
