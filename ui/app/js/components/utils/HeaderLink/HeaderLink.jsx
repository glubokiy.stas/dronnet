import React from 'react';
import {Link, Route} from 'react-router-dom';
import cn from 'classnames';

export default class HeaderLink extends React.Component {
    render() {
        return (
            <Route
                path={this.props.to}
                exact={this.props.exact}
                children={({match}) => (
                    <Link to={this.props.to} className={cn(this.props.className, {'is-active': match}, 'navbar-item')}>
                        {this.props.label}
                    </Link>
                )}
            />
        );
    }

}
