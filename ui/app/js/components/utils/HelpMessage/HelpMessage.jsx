import React from 'react';
import s from './style.scss';
import cn from 'classnames';
import PropTypes from 'prop-types';


export default class HelpMessage extends React.Component {
    static propTypes = {
        color: PropTypes.string,
        size: PropTypes.string,
        error: PropTypes.bool,
        margin: PropTypes.bool,
    };

    static defaultProps = {
        color: 'grey-light',
        size: '7',
        error: false,
        margin: false,
    };

    render () {
        let color = this.props.error ? 'danger' : this.props.color;
        let size = this.props.info ? '5' : this.props.size;
        let style = {};

        if (this.props.margin) {
            style['marginTop'] = '15px';
        }

        return (
            <span>
                {this.props.children &&
                    <div className={cn(`help has-text-${color} is-size-${size}`, s.help)} style={style}>
                        {this.props.children}
                    </div>
                }
            </span>
        );
    }
}
