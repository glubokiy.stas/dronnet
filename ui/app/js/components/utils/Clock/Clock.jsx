import React from 'react';
import moment from 'moment-timezone';
import {connect} from 'react-redux';

@connect(
    (state) => ({
        timezone: state.auth.user.timezone
    })
)
export default class Clock extends React.Component {
    constructor(props) {
        super(props);

        this.state = {time: moment()};

        this.interval = setInterval(() => {
            this.setState({time: moment()});
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        return (
            <span>{this.state.time.tz(this.props.timezone).format('HH:mm')}
                <span className="has-text-grey is-size-7"> {this.state.time.tz(this.props.timezone).format('Z')}</span>
            </span>
        );
    }
}
