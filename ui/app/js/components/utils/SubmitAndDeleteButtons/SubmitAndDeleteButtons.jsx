import React from 'react';
import PropTypes from 'prop-types';
import {UPDATE_MODE} from "../../../utils/constants";
import cn from "classnames";
import SubmitButton from "../SubmitButton/SubmitButton";
import Conditional from "../Conditional/Conditional";


export default class SubmitAndDeleteButtons extends React.Component {
    static propTypes = {
        editMode: PropTypes.string.isRequired,
        isFetching: PropTypes.bool,
        isFetchingDelete: PropTypes.bool,
        onDeleteButtonClick: PropTypes.func.isRequired,
    };

    static defaultProps = {
        isFetching: false,
        isFetchingDelete: false,
    };

    render() {
        return (
            <div className="level">
                <div className="level-left">
                    <SubmitButton isFetching={this.props.isFetching}>
                        {this.props.editMode === UPDATE_MODE ? "Save" : "Create"}
                    </SubmitButton>
                </div>
                <div className="level-right">
                    <Conditional if={this.props.editMode === UPDATE_MODE}>
                        <button className={cn(
                            "button is-dark has-text-danger",
                            {'is-loading': this.props.isFetchingDelete}
                        )}
                                type="button"
                                onClick={this.props.onDeleteButtonClick}>
                            Delete
                        </button>
                    </Conditional>
                </div>
            </div>
        );
    }
}
