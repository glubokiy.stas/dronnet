import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import s from './style.scss';
import {getDroneModelLabel} from "../../../services/droneModel";


export default class DroneBox extends React.Component {
    static propTypes = {
        id: PropTypes.number.isRequired,
        model: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        onBoxClick: PropTypes.func.isRequired,
    };

    render() {
        return (
            <div className={cn("box", s.box)} onClick={this.props.onBoxClick}>
                <p><span className="param-name">Name:</span> {this.props.name}</p>
                <p><span className="param-name">Model:</span> {getDroneModelLabel(this.props.model)}</p>
            </div>
        );
    }
}
