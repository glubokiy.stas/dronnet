import React from 'react';
import {connect} from 'react-redux';
import {push} from "react-router-redux";
import {fetchDronesAsync} from "../../redux-modules/drone/actions";
import {fetchDroneModelsAsync} from "../../redux-modules/droneModel/actions";
import DronesList from "./DronesList/DronesList";


@connect(
    (state) => ({
        drones: state.drone.items,
        errorMessage: state.drone.errorMessage,
        isFetching: state.drone.isFetching,
        isFetchingModels: state.droneModel.isFetching,
    }),
    {
        fetchDronesAsync,
        fetchDroneModelsAsync,
        changeRoute: push,
    }
)
export default class DronesListContainer extends React.Component {
    componentDidMount() {
        this.props.fetchDronesAsync();
        this.props.fetchDroneModelsAsync();
    }

    handleBoxClick = (drone) => {
        this.props.changeRoute(`/profile/update-drone/${drone.id}`);
    };

    render() {
        return (
            <DronesList
                {...this.props}
                onBoxClick={this.handleBoxClick}
            />
        );
    }
}
