import React from 'react';
import PropTypes from 'prop-types';
import {getFormData} from "../../../utils/utils";
import cn from 'classnames';
import SubmitButton from '../../utils/SubmitButton/SubmitButton';
import HelpMessage from '../../utils/HelpMessage/HelpMessage';
import Field from "../../utils/Field/Field";
import Input from "../../utils/Input/Input";

export default class SignUp extends React.Component {
    static propTypes = {
        errorMessage: PropTypes.string,
        isFetching: PropTypes.bool,
        onFormSubmit: PropTypes.func.isRequired,
    };

    static defaultProps = {
        errorMessage: '',
        isFetching: false
    };

    handleFormSubmit = (event) => {
        event.preventDefault();
        const formData = getFormData(event.target);
        this.props.onFormSubmit(formData);
    };

    render() {
        return (
            <form className="form is-dark" onSubmit={this.handleFormSubmit}>
                <Field label="Username">
                    <Input placeholder="JohnDoe" name="username"/>
                </Field>

                <Field label="Email">
                    <Input placeholder="jdoe@gmail.com" name="email"/>
                </Field>

                <Field label="Password">
                    <Input type="password" placeholder="••••••••••••" name="password" autoComplete="new-password"/>
                </Field>

                <SubmitButton isFetching={this.props.isFetching}>Sign Up</SubmitButton>

                <HelpMessage error>
                    {this.props.errorMessage}
                </HelpMessage>
            </form>
        );
    }
}
