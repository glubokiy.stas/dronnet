import {connect} from 'react-redux';
import React from "react";
import PropTypes from 'prop-types';
import {
    refreshReservationsByFilterModeAsync, reservationsHover, reservationsSetDateFilter, reservationsSetFilterMode
} from '../../redux-modules/reservation/actions';
import ReservationsListMain from "./ReservationsListMain/ReservationsListMain";
import {mapChangeBounds, resetCenterAndZoom} from "../../redux-modules/map/actions";
import {getBoundsForCoordinates} from "../../utils/utils";
import {FILTER_ALL, FILTER_OWN} from '../../redux-modules/reservation/constants';
import {push} from "react-router-redux";


@connect(
    (state) => ({
        defaultReservationDate: state.reservation.dateFilter,
        errorMessage: state.reservation.errorMessage,
        isFetching: state.reservation.isFetching,
        reservations: state.reservation.items,
        userType: state.auth.user.type
    }),
    {
        mapChangeBounds,
        refreshReservationsByFilterModeAsync,
        reservationsHover,
        reservationsSetDateFilter,
        reservationsSetFilterMode,
        changeRoute: push,
        onResetMapClick: resetCenterAndZoom
    }
)
export default class ReservationsListContainer extends React.Component {
    static propTypes = {
        onlyOwn: PropTypes.bool,
    };

    static defaultProps = {
        onlyOwn: false,
    };

    componentDidMount() {
        this.fetchReservationsAsync();

        // Update reservations periodically.
        this.updateInterval = setInterval(() => this.fetchReservationsAsync(), 30 * 1000);
    }

    componentWillUnmount() {
        // Cancel periodical update.
        clearInterval(this.updateInterval);
        this.props.reservationsSetFilterMode(null);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.onlyOwn !== this.props.onlyOwn) {
            this.fetchReservationsAsync(nextProps);
        }
    }

    fetchReservationsAsync(nextProps) {
        let props = nextProps || this.props;

        if (props.onlyOwn) {
            this.props.refreshReservationsByFilterModeAsync(FILTER_OWN);
        } else {
            this.props.refreshReservationsByFilterModeAsync(FILTER_ALL);
        }
    }

    handleEditButtonClick = (reservation) => {
        // When edit button is clicked, reservation box is being hovered,
        // so we need to trigger unhovering.
        this.props.reservationsHover(null);
        this.props.changeRoute(`/update-reservation/${reservation.id}`);
    };

    handleReservationClick = (reservation) => {
        let bounds = getBoundsForCoordinates(reservation.coordinates);
        this.props.mapChangeBounds(bounds);
    };

    handleReservationHover = (reservation) => {
        if (reservation) {
            this.props.reservationsHover(reservation.id);
        } else {
            this.props.reservationsHover(null);
        }
    };

    handleDateFilterChange = (val) => {
        this.props.reservationsSetDateFilter(val);
        this.fetchReservationsAsync();
    };

    render() {
        return (
            <ReservationsListMain
                {...this.props}
                onReservationClick={this.handleReservationClick}
                onReservationHover={this.handleReservationHover}
                onDateFilterChange={this.handleDateFilterChange}
                onEditButtonClick={this.handleEditButtonClick}
            />
        );
    }
}
