import {connect} from 'react-redux';
import React from "react";
import ReservationForm from "./ReservationForm/ReservationForm";
import {
    createReservationWithStateAsync, newResDroneDeselect, newResDroneSelect, newResFetchDronesAsync, newResResetForm,
    newResSetDefaultDates, newResSetEndDate,
    newResSetHeight,
    newResSetStartDate, setReservationForEditAsync, updateReservationWithStateAsync,
} from "../../redux-modules/newReservation/actions";
import {push} from "react-router-redux";
import {refreshReservationsByFilterModeAsync, reservationsSetFilterMode} from '../../redux-modules/reservation/actions';
import {drawResetCircle, drawResetPolygon, drawSetDrawMode, drawSetRadius} from "../../redux-modules/draw/actions";
import {CIRCLE, NODRAW} from "../../redux-modules/draw/index";
import {FILTER_PI} from '../../redux-modules/reservation/constants';
import {DT_FORMAT} from "../../redux-modules/newReservation/constants";
import {CREATE_MODE, UPDATE_MODE} from "../../utils/constants";

@connect(
    (state) => ({
        drawMode: state.draw.drawMode,
        drawnCircle: state.draw.circle,
        drawnPolygon: state.draw.polygon,
        drones: state.newReservation.drones,
        endDate: state.newReservation.endDate,
        errorMessage: state.newReservation.errorMessage,
        height: state.newReservation.height,
        isFetching: state.newReservation.isFetching,
        isFetchingDrones: state.newReservation.isFetchingDrones,
        isFetchingReservations: state.reservation.isFetching,
        startDate: state.newReservation.startDate
    }),
    {
        createReservationWithStateAsync,
        newResDroneDeselect,
        newResDroneSelect,
        newResFetchDronesAsync,
        newResResetForm,
        newResSetHeight,
        newResSetDefaultDates,
        refreshReservationsByFilterModeAsync,
        reservationsSetFilterMode,
        setReservationForEditAsync,
        updateReservationWithStateAsync,
        changeRoute: push,
        onCircleResetClick: drawResetCircle,
        onCircleRadiusChange: drawSetRadius,
        onDrawModeChange: drawSetDrawMode,
        onEndDateStringChange: newResSetEndDate,
        onPolygonResetClick: drawResetPolygon,
        onStartDateStringChange: newResSetStartDate,
    }
)
export default class ReservationFormContainer extends React.Component {
    constructor(props) {
        super(props);
        this.editMode = this.props.match.params.reservationId ? UPDATE_MODE : CREATE_MODE;
    }

    componentDidMount() {
        if (this.editMode === CREATE_MODE) {
            this.props.newResSetDefaultDates();
            this.props.onDrawModeChange(CIRCLE);

            this.fetchReservationsAsync();
            this.props.newResFetchDronesAsync();
        }

        if (this.editMode === UPDATE_MODE) {
            this.reservationId = parseInt(this.props.match.params.reservationId);
            this.props.setReservationForEditAsync(this.reservationId).then(
                () => this.fetchReservationsAsync()
            );
        }

        // Update reservations periodically.
        this.updateInterval = setInterval(() => this.fetchReservationsAsync(), 30 * 1000);
    }

    componentWillUnmount() {
        // Cancel periodical update.
        clearInterval(this.updateInterval);
        this.props.reservationsSetFilterMode(null);
        this.props.onDrawModeChange(NODRAW);

        // Reset reservation error.
        this.props.newResResetForm()
    }

    fetchReservationsAsync() {
        this.props.refreshReservationsByFilterModeAsync(FILTER_PI);
    }

    handleDroneClick = (drone) => {
        if (drone.isSelected) {
            this.props.newResDroneDeselect(drone.id);
        } else {
            this.props.newResDroneSelect(drone.id);
        }
    };

    handleFormSubmit = () => {
        if (this.editMode === CREATE_MODE) {
            this.props.createReservationWithStateAsync().then(
                () => this.props.changeRoute('/profile/reservations')
            );
        }

        if (this.editMode === UPDATE_MODE) {
            this.props.updateReservationWithStateAsync().then(
                () => this.props.changeRoute('/')
            );
        }
    };

    handleHeightChange = (val) => {
        this.props.newResSetHeight(val);
        this.fetchReservationsAsync();
    };

    handleEndDateStringChange = (val) => {
        this.props.onEndDateStringChange(val);
        this.fetchReservationsAsync();
    };

    handleStartDateStringChange = (val) => {
        this.props.onStartDateStringChange(val);
        this.fetchReservationsAsync();
    };

    render() {
        if (!this.props.startDate || !this.props.endDate) {
            return null;
        }

        return (
            <ReservationForm
                {...this.props}
                defaultStartDateString={this.props.startDate.format(DT_FORMAT)}
                defaultEndDateString={this.props.endDate.format(DT_FORMAT)}
                editMode={this.editMode}
                onDroneClick={this.handleDroneClick}
                onStartDateStringChange={this.handleStartDateStringChange}
                onEndDateStringChange={this.handleEndDateStringChange}
                onFormSubmit={this.handleFormSubmit}
                onHeightChange={this.handleHeightChange}
            />
        );
    }
}
