import React from 'react';
import PropTypes from 'prop-types';
import SectionTitle from "../../utils/SectionTitle/SectionTitle";
import Field from "../../utils/Field/Field";
import Input from "../../utils/Input/Input";
import HelpMessage from "../../utils/HelpMessage/HelpMessage";
import {CREATE_MODE, UPDATE_MODE} from "../../../utils/constants";
import SubmitAndDeleteButtons from "../../utils/SubmitAndDeleteButtons/SubmitAndDeleteButtons";


export default class DroneModelForm extends React.Component {
    static propTypes = {
        editMode: PropTypes.string,
        errorMessage: PropTypes.string,
        isFetching: PropTypes.bool,
        isFetchingDetails: PropTypes.bool,
        isFetchingDelete: PropTypes.bool,
        manufacturer: PropTypes.string.isRequired,
        modelName: PropTypes.string.isRequired,
        onFormSubmit: PropTypes.func.isRequired,
        onManufacturerChange: PropTypes.func.isRequired,
        onModelNameChange: PropTypes.func.isRequired,
        onDeleteButtonClick: PropTypes.func.isRequired,
    };

    static defaultProps = {
        editMode: CREATE_MODE,
        errorMessage: '',
        isFetching: false,
        isFetchingDetails: false,
        isFetchingDelete: false,
    };

    handleFormSubmit = (event) => {
        event.preventDefault();

        this.props.onFormSubmit();
    };

    render() {
        return (
            <form className="form is-dark" onSubmit={this.handleFormSubmit}>
                <SectionTitle
                    text={this.props.editMode === UPDATE_MODE ? "Update Drone Model" : "Create Drone Model"}
                    withSpinner={this.props.isFetchingDetails}
                />

                <Field label="Manufacturer">
                    <Input
                        placeholder="DJI"
                        value={this.props.manufacturer}
                        onChange={(e) => this.props.onManufacturerChange(e.target.value)}
                    />
                </Field>

                <Field label="Model Name">
                    <Input
                        placeholder="Phantom 4"
                        value={this.props.modelName}
                        onChange={(e) => this.props.onModelNameChange(e.target.value)}
                    />
                </Field>

                <SubmitAndDeleteButtons
                    editMode={this.props.editMode}
                    isFetching={this.props.isFetching}
                    isFetchingDelete={this.props.isFetchingDelete}
                    onDeleteButtonClick={this.props.onDeleteButtonClick}
                />

                <HelpMessage error>
                    {this.props.errorMessage}
                </HelpMessage>
            </form>
        );
    }
}
