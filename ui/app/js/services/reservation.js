import axios from 'axios';
import {apiUrl} from '../redux-modules/config';

export function getReservationsAsync({startDate, endDate, height, north, west, south, east, onlyOwn}) {
    let url = `${apiUrl}/reservations/`;
    if (onlyOwn) {
        url = `${apiUrl}/reservations/own/`;
    }
    return axios.get(url, {params: {startDate, endDate, height, north, west, south, east}});
}

export function getReservationDetailsAsync(id) {
    return axios.get(`${apiUrl}/reservations/${id}/`);
}

export function createNewReservationAsync({startDate, endDate, height, coordinates, radius, droneIds}) {
    return axios.post(`${apiUrl}/reservations/`, {startDate, endDate, height, coordinates, radius, droneIds});
}

export function updateReservationAsync(id, {startDate, endDate, height, coordinates, radius, droneIds}) {
    return axios.patch(`${apiUrl}/reservations/${id}/`, {startDate, endDate, height, coordinates, radius, droneIds});
}
