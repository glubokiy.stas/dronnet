import axios from 'axios';
import {apiUrl} from '../redux-modules/config';
import {store} from '../App';

export function fetchDroneModelsAsync() {
    return axios.get(`${apiUrl}/drone-models/`);
}

export function fetchDroneModelDetails(id) {
    return axios.get(`${apiUrl}/drone-models/${id}/`);
}

export function createDroneModelAsync({manufacturer, modelName}) {
    return axios.post(`${apiUrl}/drone-models/`, {manufacturer, modelName})
}

export function updateDroneModelAsync(id, {manufacturer, modelName}) {
    return axios.patch(`${apiUrl}/drone-models/${id}/`, {manufacturer, modelName})
}

export function deleteDroneModelAsync(id) {
    return axios.delete(`${apiUrl}/drone-models/${id}/`)
}

export function getDroneModelLabel(id) {
    const droneModels = store.getState().droneModel.items;
    const droneModel = droneModels.find((d) => d.id === id);
    if (droneModel) {
        return `${droneModel.manufacturer} ${droneModel.modelName}`;
    } else {
        return 'N/A';
    }
}
