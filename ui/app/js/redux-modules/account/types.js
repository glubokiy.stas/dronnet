export const ACCOUNT_REQUEST = 'account/REQUEST';
export const ACCOUNT_RECEIVE = 'account/RECEIVE';
export const ACCOUNT_ERROR = 'account/ERROR';

export const ACCOUNT_SET_FIRST_NAME = 'account/SET_FIRSTNAME';
export const ACCOUNT_SET_LAST_NAME = 'account/SET_LASTNAME';
export const ACCOUNT_SET_ADDRESS = 'account/SET_ADDRESS';
export const ACCOUNT_SET_PHONE = 'account/SET_PHONE';
export const ACCOUNT_SET_TIMEZONE = 'account/SET_TIMEZONE';
export const ACCOUNT_RESET_FORM = 'account/RESET_FORM';
