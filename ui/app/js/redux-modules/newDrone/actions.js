import * as types from './types';
import {errorDispatchAndReject} from '../../utils/utils';
import * as droneService from "../../services/drone";


export function newDroneRequest() {
    return {
        type: types.NEW_DRONE_REQUEST
    }
}

export function newDroneReceive() {
    return {
        type: types.NEW_DRONE_RECEIVE
    }
}

export function newDroneError(message) {
    return {
        type: types.NEW_DRONE_ERROR,
        message
    }
}

export function newDroneRequestDetails() {
    return {
        type: types.NEW_DRONE_REQUEST_DETAILS
    }
}

export function newDroneReceiveDetails() {
    return {
        type: types.NEW_DRONE_RECEIVE_DETAILS
    }
}

export function newDroneErrorDetails(message) {
    return {
        type: types.NEW_DRONE_ERROR_DETAILS,
        message
    }
}

export function newDroneRequestDelete() {
    return {
        type: types.NEW_DRONE_REQUEST_DELETE
    }
}

export function newDroneReceiveDelete() {
    return {
        type: types.NEW_DRONE_RECEIVE_DELETE
    }
}

export function newDroneErrorDelete(message) {
    return {
        type: types.NEW_DRONE_ERROR_DELETE,
        message
    }
}

export function newDroneSetName(name) {
    return {
        type: types.NEW_DRONE_SET_NAME,
        name
    }
}

export function newDroneSetModel(model) {
    return {
        type: types.NEW_DRONE_SET_MODEL,
        model
    }
}

export function createDroneAsync() {
    return (dispatch, getState) => {
        const state = getState();
        const model = state.newDrone.model;
        const name = state.newDrone.name;

        dispatch(newDroneRequest());

        return droneService.createDroneAsync({model, name}).then(
            (response) => {
                dispatch(newDroneReceive());
                return response;
            },
            errorDispatchAndReject(dispatch, newDroneError, 'Unable to create a drone')
        );
    };
}

export function updateDroneAsync(id) {
    return (dispatch, getState) => {
        const state = getState();
        const model = state.newDrone.model;
        const name = state.newDrone.name;

        dispatch(newDroneRequest());
        return droneService.updateDroneAsync(id, {model, name}).then(
            (response) => {
                dispatch(newDroneReceive());
                return response;
            },
            errorDispatchAndReject(dispatch, newDroneError, 'Unable to update the drone')
        );
    };
}

export function deleteDroneAsync(id) {
    return (dispatch) => {
        dispatch(newDroneRequestDelete());
        return droneService.deleteDroneAsync(id).then(
            (response) => {
                dispatch(newDroneReceiveDelete());
                return response;
            },
            errorDispatchAndReject(dispatch, newDroneErrorDelete, 'Unable to delete the drone')
        );
    };
}

export function setDroneForEditAsync(id) {
    return (dispatch) => {
        dispatch(newDroneRequestDetails());
        return droneService.fetchDroneDetails(id).then(
            (response) => {
                dispatch(newDroneReceiveDetails());
                const drone = response.data;

                dispatch(newDroneSetModel(drone.model));
                dispatch(newDroneSetName(drone.name));
            },
            errorDispatchAndReject(dispatch, newDroneErrorDetails, 'Unable to fetch the drone')
        );
    }
}

export function newDroneResetForm() {
    return {
        type: types.NEW_DRONE_RESET_FORM
    }
}
