export const AUTH_LOGIN_REQUEST = 'auth/LOGIN_REQUEST';
export const AUTH_LOGIN_ERROR = 'auth/LOGIN_ERROR';
export const AUTH_USER_RECEIVE = 'auth/USER_RECEIVE';
export const AUTH_USER_ERROR = 'auth/USER_ERROR';
