export const SIGNUP_REQUEST = 'signUp/REQUEST';
export const SIGNUP_RECEIVE = 'signUp/RECEIVE';
export const SIGNUP_ERROR = 'signUp/ERROR';
