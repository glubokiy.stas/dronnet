import * as types from "./types";


const initialState = {
    errorMessage: '',
    isFetching: false,
    isSignedUp: false
};


export default function signUp(state = initialState, action) {
    switch (action.type) {

        case types.SIGNUP_REQUEST:
            return {
                ...state,
                isFetching: true
            };

        case types.SIGNUP_RECEIVE:
            return {
                ...state,
                isFetching: false,
                errorMessage: '',
                isSignedUp: true
            };

        case types.SIGNUP_ERROR:
            return {
                ...state,
                isFetching: false,
                errorMessage: action.message,
                isSignedUp: false
            };

        default:
            return state
    }
}
