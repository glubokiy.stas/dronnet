import * as types from './types';


const initialState = {
    errorMessage: '',
    isFetching: false,
    items: [],
};


export default function drone(state = initialState, action) {
    switch (action.type) {

        case types.DRONE_REQUEST:
            return {
                ...state,
                isFetching: true,
                items: [],
            };

        case types.DRONE_RECEIVE:
            return {
                ...state,
                isFetching: false,
                errorMessage: '',
                items: action.items,
            };

        case types.DRONE_ERROR:
            return {
                ...state,
                isFetching: false,
                errorMessage: action.message
            };

        default:
            return state
    }
}
