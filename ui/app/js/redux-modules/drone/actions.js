import * as types from './types';
import {errorDispatchAndReject} from '../../utils/utils';
import * as droneService from "../../services/drone";


export function droneRequest() {
    return {
        type: types.DRONE_REQUEST
    }
}

export function droneReceive(items) {
    return {
        type: types.DRONE_RECEIVE,
        items
    }
}

export function droneError(message) {
    return {
        type: types.DRONE_ERROR,
        message
    }
}

export function fetchDronesAsync() {
    return (dispatch) => {
        dispatch(droneRequest());

        return droneService.fetchDronesAsync().then(
            (response) => {
                dispatch(droneReceive(response.data));
                return response;
            },
            errorDispatchAndReject(dispatch, droneError, 'Unable to load drones')
        );
    }
}
