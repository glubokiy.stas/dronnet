import * as types from './types';
import moment from 'moment-timezone';
import {errorDispatchAndReject, getBoundsParamsFromState} from '../../utils/utils';
import * as reservationService from '../../services/reservation';
import {fetchReservationsAsync} from '../reservation/actions';
import {CIRCLE, POLYGON} from "../draw";
import {drawSetCenter, drawSetCoordinates, drawSetDrawMode, drawSetRadius} from "../draw/actions";
import {getDataFromState} from "./utils";
import {DT_FORMAT} from "./constants";
import * as droneService from "../../services/drone";


export function newResSetStartDate(date) {
    return (dispatch, getState) => {
        dispatch({
            type: types.NEW_RES_SET_START_DATE,
            date: typeof date === 'string' ? moment.tz(date, DT_FORMAT, getState().auth.user.timezone) : date
        });
    }
}

export function newResSetEndDate(date) {
    return (dispatch, getState) => {
        dispatch({
            type: types.NEW_RES_SET_END_DATE,
            date: typeof date === 'string' ? moment.tz(date, DT_FORMAT, getState().auth.user.timezone) : date
        });
    }
}

export function newResSetDefaultDates() {
    return (dispatch, getState) => {
        dispatch(newResSetStartDate(moment().tz(getState().auth.user.timezone).add(1, 'hours').startOf('hour')));
        dispatch(newResSetEndDate(moment().tz(getState().auth.user.timezone).add(2, 'hours').startOf('hour')));
    }
}

export function newResSetHeight(height) {
    return {
        type: types.NEW_RES_SET_HEIGHT,
        height
    }
}

export function newReservationRequest() {
    return {
        type: types.NEW_RES_REQUEST
    }
}

export function newReservationReceive() {
    return {
        type: types.NEW_RES_RECEIVE
    }
}

export function newReservationError(message) {
    return {
        type: types.NEW_RES_ERROR,
        message
    }
}

export function newResSetIdToFilter(id) {
    return {
        type: types.NEW_RES_SET_FILTER_ID,
        id
    }
}

export function newResResetForm() {
    return {
        type: types.NEW_RES_RESET_FORM
    }
}

export function newResDroneRequest() {
    return {
        type: types.NEW_RES_DRONE_REQUEST
    }
}

export function newResDroneReceive(items) {
    return {
        type: types.NEW_RES_DRONE_RECEIVE,
        items
    }
}

export function newResDroneError(message) {
    return {
        type: types.NEW_RES_DRONE_ERROR,
        message
    }
}

export function newResDroneSelect(droneId) {
    return {
        type: types.NEW_RES_DRONE_SELECT,
        droneId
    }
}

export function newResDroneDeselect(droneId) {
    return {
        type: types.NEW_RES_DRONE_DESELECT,
        droneId
    }
}

export function newResFetchDronesAsync() {
    return (dispatch) => {
        dispatch(newResDroneRequest());

        return droneService.fetchDronesAsync().then(
            (response) => {
                const drones = response.data.map((d) => ({...d, isSelected: false}));
                dispatch(newResDroneReceive(drones));
                return response;
            },
            errorDispatchAndReject(dispatch, newResDroneError, 'Unable to load drones')
        );
    }
}

export function createReservationAsync(data) {
    return (dispatch) => {
        dispatch(newReservationRequest());
        return reservationService.createNewReservationAsync(data).then(
            (response) => {
                dispatch(newReservationReceive());
                return response;
            },
            errorDispatchAndReject(dispatch, newReservationError, 'Unable to create new reservation')
        );
    };
}

export function updateReservationAsync(id, data) {
    return (dispatch) => {
        dispatch(newReservationRequest());
        return reservationService.updateReservationAsync(id, data).then(
            (response) => {
                dispatch(newReservationReceive());
                return response;
            },
            errorDispatchAndReject(dispatch, newReservationError, 'Unable to save reservation')
        );
    };
}

export function createReservationWithStateAsync() {
    return (dispatch, getState) => {
        const state = getState();
        const data = getDataFromState(dispatch, state);

        if (!data) {
            return Promise.reject();
        }

        return dispatch(createReservationAsync(data));
    };
}

export function updateReservationWithStateAsync() {
    return (dispatch, getState) => {
        const state = getState();
        const data = getDataFromState(dispatch, state);

        if (!data) {
            return Promise.reject();
        }

        return dispatch(updateReservationAsync(state.newReservation.reservationIdToFilter, data));
    };
}

// PIReservations refers to Possibly Intersecting Reservations
// which means that these reservations have intersecting intervals with new reservation
// and possibly intersecting zones with it.
export function fetchPIReservationsAsync() {
    return (dispatch, getState) => {
        const state = getState();

        let startDate = state.newReservation.startDate;
        let endDate = state.newReservation.endDate;

        if (!startDate || !startDate.isValid() || !endDate || !endDate.isValid()) {
            return;
        }

        let boundsParams = getBoundsParamsFromState(state);
        let height = state.newReservation.height;

        return dispatch(fetchReservationsAsync(
            {startDate, endDate, boundsParams, height},
            state.newReservation.reservationIdToFilter
        ));
    }
}

export function setReservationForEditAsync(id) {
    return (dispatch) => {
        return reservationService.getReservationDetailsAsync(id).then(
            (response) => {
                const reservation = response.data;

                dispatch(newResSetStartDate(reservation.startDate));
                dispatch(newResSetEndDate(reservation.endDate));
                dispatch(newResSetHeight(reservation.height));
                dispatch(newResSetIdToFilter(reservation.id));

                if (reservation.radius) {
                    dispatch(drawSetDrawMode(CIRCLE));
                    dispatch(drawSetRadius(reservation.radius));
                    dispatch(drawSetCenter(reservation.center));
                } else {
                    dispatch(drawSetDrawMode(POLYGON));
                    dispatch(drawSetCoordinates(reservation.coordinates));
                }

                dispatch(newResDroneRequest());
                droneService.fetchDronesForUserAsync(reservation.userId).then(
                    (response) => {
                        const drones = response.data.map((drone) =>
                            ({...drone, isSelected: reservation.drones.find((resDrone) => resDrone.id === drone.id)})
                        );
                        dispatch(newResDroneReceive(drones));
                    },
                    errorDispatchAndReject(dispatch, newResDroneError, 'Unable to load drones')
                )
            }
        )
    }
}
