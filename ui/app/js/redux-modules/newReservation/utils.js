import {CIRCLE, POLYGON} from "../draw";
import {newReservationError} from "./actions";
import {coordinatesToTuples} from "../../utils/utils";

export function getDataFromState(dispatch, state) {
    const newReservation = state.newReservation;
    const draw = state.draw;

    let data = {};
    data.height = newReservation.height;

    data.startDate = newReservation.startDate;
    if (!data.startDate.isValid()) {
        dispatch(newReservationError('Start Time: invalid value'));
        return;
    }

    data.endDate = newReservation.endDate;
    if (!data.endDate.isValid()) {
        dispatch(newReservationError('End Time: invalid value'));
        return;
    }

    if (draw.drawMode === CIRCLE) {
        data.coordinates = [draw.circle.center];
        data.radius = draw.circle.radius;
    } else if (draw.drawMode === POLYGON) {
        data.coordinates = draw.polygon.coordinates;
    } else {
        dispatch(newReservationError('Reservation shape is not set'));
        return;
    }

    if (!data.coordinates[0] || !data.coordinates[0].lat) {
        dispatch(newReservationError('Zone is not set'));
        return;
    }

    data.coordinates = coordinatesToTuples(data.coordinates);

    data.droneIds = state.newReservation.drones.filter((d) => d.isSelected).map((d) => d.id);

    if (!data.droneIds.length) {
        dispatch(newReservationError('Choose at least 1 drone'));
        return;
    }

    return data;
}
