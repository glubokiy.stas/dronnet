import * as types from './types';
import {errorDispatchAndReject, getBoundsParamsFromState} from '../../utils/utils';
import {mapSaveBounds} from '../map/actions';
import * as reservationService from '../../services/reservation';
import {fetchPIReservationsAsync} from '../newReservation/actions';
import {NODRAW} from "../draw";
import {FILTER_ALL, FILTER_OWN, FILTER_PI} from './constants';


export function reservationsRequest() {
    return {
        type: types.RESERVATIONS_REQUEST
    }
}

export function reservationsReceive(items) {
    return {
        type: types.RESERVATIONS_RECEIVE,
        items
    }
}

export function reservationsError(message) {
    return {
        type: types.RESERVATIONS_ERROR,
        message
    }
}

export function reservationsSetDateFilter(date) {
    return {
        type: types.RESERVATIONS_SET_DATE_FILTER,
        dateFilter: date
    }
}

export function reservationsSetFilterMode(filterMode) {
    return {
        type: types.RESERVATIONS_SET_FILTER_MODE,
        filterMode
    }
}

export function reservationsHover(itemId) {
    return {
        type: types.RESERVATIONS_HOVER,
        itemId
    }
}

export function fetchReservationsAsync({startDate, endDate, height, boundsParams, onlyOwn}, reservationIdToFilter) {
    return (dispatch) => {
        dispatch(reservationsRequest());

        return reservationService.getReservationsAsync({startDate, endDate, height, ...boundsParams, onlyOwn}).then(
            (response) => {
                const reservations = response.data.filter((r) => r.id !== reservationIdToFilter);
                dispatch(reservationsReceive(reservations));
                return response;
            },
            errorDispatchAndReject(dispatch, reservationsError, 'Unable to load reservations')
        );
    }
}

export function fetchAllReservationsAsync() {
    return (dispatch, getState) => {
        const state = getState();

        let startDate = state.reservation.dateFilter.clone().startOf('day');
        let endDate = state.reservation.dateFilter.clone().endOf('day');
        let boundsParams = getBoundsParamsFromState(state);

        return dispatch(fetchReservationsAsync({startDate, endDate, boundsParams}));
    };
}

export function fetchOwnReservationsAsync() {
    return (dispatch, getState) => {
        const state = getState();

        let startDate = state.reservation.dateFilter.clone().startOf('day');
        let endDate = state.reservation.dateFilter.clone().endOf('day');
        let boundsParams = getBoundsParamsFromState(state);

        return dispatch(fetchReservationsAsync({startDate, endDate, boundsParams, onlyOwn: true}));
    };
}

export function fetchReservationsWithFilterAsync() {
    return (dispatch, getState) => {
        const filterMode = getState().reservation.filterMode;

        switch (filterMode) {
            case FILTER_ALL:
                return dispatch(fetchAllReservationsAsync());

            case FILTER_PI:
                return dispatch(fetchPIReservationsAsync());

            case FILTER_OWN:
                return dispatch(fetchOwnReservationsAsync());
        }
    }
}

export function refreshReservationsByFilterModeAsync(filterMode) {
    return (dispatch) => {
        dispatch(reservationsSetFilterMode(filterMode));
        return dispatch(fetchReservationsWithFilterAsync());
    }
}

export function refreshReservationsByBoundsAsync(bounds) {
    return (dispatch) => {
        dispatch(mapSaveBounds(bounds));
        return dispatch(fetchReservationsWithFilterAsync());
    };
}
