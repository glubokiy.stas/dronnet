export const HOVER_FILL_COLOR = '#ce0051';
export const HOVER_STROKE_COLOR = '#7f003a';

export const FILTER_ALL = 'ALL';
export const FILTER_PI = 'PI';
export const FILTER_OWN = 'OWN';
