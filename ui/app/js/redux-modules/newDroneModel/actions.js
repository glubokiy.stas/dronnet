import * as types from './types';
import {errorDispatchAndReject} from '../../utils/utils';
import * as droneModelService from '../../services/droneModel';


export function newDroneModelRequest() {
    return {
        type: types.NEW_DMODEL_REQUEST
    }
}

export function newDroneModelReceive() {
    return {
        type: types.NEW_DMODEL_RECEIVE
    }
}

export function newDroneModelError(message) {
    return {
        type: types.NEW_DMODEL_ERROR,
        message
    }
}

export function newDroneModelRequestDetails() {
    return {
        type: types.NEW_DMODEL_REQUEST_DETAILS
    }
}

export function newDroneModelReceiveDetails() {
    return {
        type: types.NEW_DMODEL_RECEIVE_DETAILS
    }
}

export function newDroneModelErrorDetails(message) {
    return {
        type: types.NEW_DMODEL_ERROR_DETAILS,
        message
    }
}

export function newDroneModelRequestDelete() {
    return {
        type: types.NEW_DMODEL_REQUEST_DELETE
    }
}

export function newDroneModelReceiveDelete() {
    return {
        type: types.NEW_DMODEL_RECEIVE_DELETE
    }
}

export function newDroneModelErrorDelete(message) {
    return {
        type: types.NEW_DMODEL_ERROR_DELETE,
        message
    }
}

export function newDroneModelSetManufacturer(manufacturer) {
    return {
        type: types.NEW_DMODEL_SET_MANUFACTURER,
        manufacturer
    }
}

export function newDroneModelSetModelName(modelName) {
    return {
        type: types.NEW_DMODEL_SET_MODEL_NAME,
        modelName
    }
}

export function createDroneModelAsync() {
    return (dispatch, getState) => {
        const state = getState();
        const manufacturer = state.newDroneModel.manufacturer;
        const modelName = state.newDroneModel.modelName;

        dispatch(newDroneModelRequest());

        return droneModelService.createDroneModelAsync({manufacturer, modelName}).then(
            (response) => {
                dispatch(newDroneModelReceive());
                return response;
            },
            errorDispatchAndReject(dispatch, newDroneModelError, 'Unable to create drone model')
        );
    };
}

export function updateDroneModelAsync(id) {
    return (dispatch, getState) => {
        const state = getState();
        const manufacturer = state.newDroneModel.manufacturer;
        const modelName = state.newDroneModel.modelName;

        dispatch(newDroneModelRequest());
        return droneModelService.updateDroneModelAsync(id, {manufacturer, modelName}).then(
            (response) => {
                dispatch(newDroneModelReceive());
                return response;
            },
            errorDispatchAndReject(dispatch, newDroneModelError, 'Unable to update drone model')
        );
    };
}

export function deleteDroneModelAsync(id) {
    return (dispatch) => {
        dispatch(newDroneModelRequestDelete());
        return droneModelService.deleteDroneModelAsync(id).then(
            (response) => {
                dispatch(newDroneModelReceiveDelete());
                return response;
            },
            errorDispatchAndReject(dispatch, newDroneModelErrorDelete, 'Unable to delete drone model')
        );
    };
}

export function setDroneModelForEditAsync(id) {
    return (dispatch) => {
        dispatch(newDroneModelRequestDetails());
        return droneModelService.fetchDroneModelDetails(id).then(
            (response) => {
                dispatch(newDroneModelReceiveDetails());
                const droneModel = response.data;

                dispatch(newDroneModelSetManufacturer(droneModel.manufacturer));
                dispatch(newDroneModelSetModelName(droneModel.modelName));
            },
            errorDispatchAndReject(dispatch, newDroneModelErrorDetails, 'Unable to fetch drone model')
        );
    }
}

export function newDroneModelResetForm() {
    return {
        type: types.NEW_DMODEL_RESET_FORM
    }
}
