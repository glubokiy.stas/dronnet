import * as types from './types';
import {errorDispatchAndReject} from '../../utils/utils';
import * as droneModelService from '../../services/droneModel';


export function droneModelRequest() {
    return {
        type: types.DRONE_MODEL_REQUEST
    }
}

export function droneModelReceive(items) {
    return {
        type: types.DRONE_MODEL_RECEIVE,
        items
    }
}

export function droneModelError(message) {
    return {
        type: types.DRONE_MODEL_ERROR,
        message
    }
}

export function fetchDroneModelsAsync() {
    return (dispatch) => {
        dispatch(droneModelRequest());

        return droneModelService.fetchDroneModelsAsync().then(
            (response) => {
                dispatch(droneModelReceive(response.data));
                return response;
            },
            errorDispatchAndReject(dispatch, droneModelError, 'Unable to load drone models')
        );
    }
}
