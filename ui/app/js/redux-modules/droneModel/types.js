export const DRONE_MODEL_REQUEST = 'droneModel/REQUEST';
export const DRONE_MODEL_RECEIVE = 'droneModel/RECEIVE';
export const DRONE_MODEL_ERROR = 'droneModel/ERROR';
