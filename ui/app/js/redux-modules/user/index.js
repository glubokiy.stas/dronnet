import * as types from './types';


const initialState = {
    errorMessage: '',
    isFetching: false,
    items: [],
};


export default function users(state = initialState, action) {
    switch (action.type) {

        case types.USERS_REQUEST:
            return {
                ...state,
                isFetching: true,
                items: [],
            };

        case types.USERS_RECEIVE:
            return {
                ...state,
                isFetching: false,
                errorMessage: '',
                items: action.items,
            };

        case types.USERS_ERROR:
            return {
                ...state,
                isFetching: false,
                errorMessage: action.message
            };

        default:
            return state
    }
}
