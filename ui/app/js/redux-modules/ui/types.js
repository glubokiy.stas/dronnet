export const UI_OPEN_SECTION = 'ui/OPEN_SECTION';
export const UI_CLOSE_SECTION = 'ui/CLOSE_SECTION';
export const UI_OPEN_LEGEND = 'ui/OPEN_LEGEND';
export const UI_CLOSE_LEGEND = 'ui/CLOSE_LEGEND';
