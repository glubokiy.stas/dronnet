export const DRAW_SET_DRAW_MODE = 'draw/SET_DRAW_MODE';
export const DRAW_SET_RADIUS = 'draw/SET_RADIUS';
export const DRAW_SET_CENTER = 'draw/SET_CENTER';
export const DRAW_SET_COORDINATES = 'draw/SET_COORDINATES';
export const DRAW_RESET_CIRCLE = 'draw/RESET_CIRCLE';
export const DRAW_RESET_POLYGON = 'draw/RESET_POLYGON';
