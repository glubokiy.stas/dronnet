import pytest
from rest_framework import status


@pytest.mark.django_db
def test_update_drone_model_by_admin(admin_client, db_drone_model):
    response = admin_client.patch('/api/drone-models/{}/'.format(db_drone_model.id), {'modelName': 'Mavic Pro'})
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_update_drone_model_by_regular_user(user_client, db_drone_model):
    response = user_client.patch('/api/drone-models/{}/'.format(db_drone_model.id), {'modelName': 'Mavic Pro'})
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_delete_drone_model_by_admin(admin_client, db_drone_model):
    response = admin_client.delete('/api/drone-models/{}/'.format(db_drone_model.id))
    assert response.status_code == status.HTTP_204_NO_CONTENT


@pytest.mark.django_db
def test_delete_drone_model_by_regular_user(user_client, db_drone_model):
    response = user_client.delete('/api/drone-models/{}/'.format(db_drone_model.id))
    assert response.status_code == status.HTTP_403_FORBIDDEN
