import pytest
from rest_framework import status


@pytest.mark.django_db
def test_retrieve_own_drones(user_client):
    response = user_client.get('/api/drones/')
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_retrieve_someones_drones_by_admin(admin_client, db_owner):
    response = admin_client.get('/api/drones/?user_id={}'.format(db_owner.user.id))
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_retrieve_someones_drones_by_regular_user(user_client, db_admin_owner):
    response = user_client.get('/api/drones/?user_id={}'.format(db_admin_owner.user.id))
    assert response.status_code == status.HTTP_400_BAD_REQUEST
