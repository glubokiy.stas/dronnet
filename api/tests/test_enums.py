from rest_framework import status


def test_retrieve_reservations_by_anon(anon_client):
    response = anon_client.get('/api/reservation-heights/')
    assert response.status_code == status.HTTP_200_OK
