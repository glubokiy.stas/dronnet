import pytest
from rest_framework import status


@pytest.mark.django_db
def test_retrieve_reservations_by_regular_user(user_client, db_user_reservation, reservation_query_params_full_map):
    response = user_client.get('/api/reservations/' + reservation_query_params_full_map)
    print(reservation_query_params_full_map)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 1


@pytest.mark.django_db
def test_retrieve_reservations_by_anon(anon_client, reservation_query_params_full_map):
    response = anon_client.get('/api/reservations/' + reservation_query_params_full_map)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_retrieve_reservation_on_edge(user_client, db_user_reservation_australia, reservation_query_params_on_edge):
    response = user_client.get('/api/reservations/' + reservation_query_params_on_edge)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 1


@pytest.mark.django_db
def test_retrieve_own_reservations(admin_client, db_user_reservation, reservation_query_params_full_map):
    response = admin_client.get('/api/reservations/own/' + reservation_query_params_full_map)
    print(reservation_query_params_full_map)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 0
