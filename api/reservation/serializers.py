from datetime import timedelta

from django.contrib.gis.geos import Polygon
from django.utils import timezone
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from api.drone.models import Drone
from api.drone.serializers import DroneSerializer
from api.reservation.constants import MAX_RESERVATION_INTERVAL
from api.reservation.models import Reservation
from api.utils import coordinates_to_lnglat, get_circle_coordinates, coordinates_lnglat_to_dict, get_distance_by_coords


class ReservationSerializer(serializers.ModelSerializer):
    coordinates = serializers.ListField(child=serializers.ListField(child=serializers.FloatField()), write_only=True)
    radius = serializers.FloatField(min_value=1, write_only=True, required=False)
    owner_username = serializers.CharField(source='owner.user.username', read_only=True)
    drones = serializers.SerializerMethodField()
    drone_ids = serializers.ListField(child=serializers.IntegerField(), write_only=True)

    class Meta:
        model = Reservation
        read_only_fields = ('zone',)
        exclude = ('owner',)
        extra_kwargs = {'height': {'default': Reservation.Heights.VARIABLE}}

    def get_drones(self, obj):
        return [str(drone.model) for drone in obj.drones.all()]

    def get_from_data_or_instance(self, data, attr):
        return data[attr] if attr in data else getattr(self.instance, attr)

    def validate(self, data):
        start_date = self.get_from_data_or_instance(data, 'start_date')
        end_date = self.get_from_data_or_instance(data, 'end_date')
        zone = self.get_from_data_or_instance(data, 'zone')
        height = self.get_from_data_or_instance(data, 'height')

        id = self.instance.id if self.instance else None

        if start_date > end_date:
            raise ValidationError('Interval end is prior to start')

        if (end_date - start_date).total_seconds() / 60 > MAX_RESERVATION_INTERVAL:
            raise ValidationError('Reservation is too long. {} minutes is max'.format(int(MAX_RESERVATION_INTERVAL)))

        if start_date < timezone.now():
            raise ValidationError('Interval start is in the past')

        if zone.area > 0.00068:
            raise ValidationError('Reservation area is too large (5 sq km is maximum).')

        if Reservation.objects.get_intersections(start_date, end_date, zone, height, id).exists():
            raise ValidationError('Provided zone intersects with existing reservations')

        return data

    def to_internal_value(self, data):
        data = super(ReservationSerializer, self).to_internal_value(data)

        drone_ids = data.pop('drone_ids', None)

        if drone_ids is not None:
            if not drone_ids:
                raise ValidationError({'drones': 'Reservation must be assigned to at least one drone'})

            # When admin is editing someone else's reservation, filtering must be done with this user's profile.
            reservation_owner = self.context['request'].user.profile if self.instance is None else self.instance.owner
            data['drones'] = Drone.objects.filter(id__in=drone_ids, owner=reservation_owner)

            if len(drone_ids) != len(data['drones']):
                raise ValidationError({'drones': 'Invalid drone id'})

        coordinates = data.pop('coordinates', None)
        radius = data.pop('radius', None)

        if coordinates is not None:
            if any(len(coordinate) != 2 or not -90 < coordinate[0] < 90 or not -180 < coordinate[1] < 180
                   for coordinate in coordinates):
                raise ValidationError({'coordinates': 'Wrong coordinates format'})

            if radius is not None and len(coordinates) != 1:
                raise ValidationError({'coordinates': 'Reservation center is not set'})

            if radius is None and len(coordinates) == 1:
                raise ValidationError({'radius': 'Reservation radius is not set'})

            if radius is None and len(coordinates) < 3:
                raise ValidationError({'coordinates': 'Too little points in reservation polygon'})

            # Coordinates are passed as LatLon pairs, whilst LonLat format is used in database.
            coordinates = coordinates_to_lnglat(coordinates)

            # If polygon is passed as a point and radius, get coordinates of this circle.
            if radius is not None:
                coordinates = get_circle_coordinates(coordinates[0], radius)

                data['shape'] = Reservation.Shape.CIRCLE
            else:
                data['shape'] = Reservation.Shape.POLYGON

            coordinates += [coordinates[0]]
            data['zone'] = Polygon(coordinates)

        return data

    def to_representation(self, instance):
        representation = super(ReservationSerializer, self).to_representation(instance)
        representation.pop('zone')

        if instance.shape == Reservation.Shape.CIRCLE:
            extent = instance.zone.extent
            avg_lat = (extent[1] + extent[3]) / 2
            representation['radius'] = round(get_distance_by_coords(avg_lat, extent[0], avg_lat, extent[2]) / 2)

        # Front-end uses LatLng notation.
        representation['coordinates'] = coordinates_lnglat_to_dict(instance.zone.coords[0])

        centroid = instance.zone.centroid
        representation['center'] = {'lat': centroid.y, 'lng': centroid.x}

        return representation


class ReservationDetailsSerializer(ReservationSerializer):
    user_id = serializers.IntegerField(source='owner.user.id', read_only=True)
    drones = DroneSerializer(many=True)


class TrafficRequestSerializer(serializers.Serializer):
    north = serializers.FloatField(min_value=-90, max_value=90)
    west = serializers.FloatField(min_value=-180, max_value=180)
    south = serializers.FloatField(min_value=-90, max_value=90)
    east = serializers.FloatField(min_value=-180, max_value=180)
    start_date = serializers.DateTimeField(default=timezone.now())
    end_date = serializers.DateTimeField(default=timezone.now() + timedelta(days=1))
    height = serializers.ChoiceField(choices=Reservation.Heights.choices(), default=Reservation.Heights.VARIABLE)
