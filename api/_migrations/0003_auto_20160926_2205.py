# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-09-26 22:05
from __future__ import unicode_literals

import django.contrib.gis.db.models.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20160922_2335'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='flightzonereservation',
            name='center',
        ),
        migrations.RemoveField(
            model_name='flightzonereservation',
            name='radius',
        ),
        migrations.AddField(
            model_name='flightzonereservation',
            name='zone',
            field=django.contrib.gis.db.models.fields.PolygonField(default='SRID=4326;POLYGON((0 0, 0 1, 1 0, 0 0))', srid=4326),
        ),
    ]
