from django.contrib.gis.db import models

from api.owner.models import DroneOwnerProfile


class DroneModel(models.Model):
    manufacturer = models.CharField(max_length=50)
    model_name = models.CharField(max_length=127, blank=True)

    class Meta:
        ordering = ('manufacturer', 'model_name')

    def __str__(self):
        return '{} {}'.format(self.manufacturer, self.model_name)


class Drone(models.Model):
    name = models.CharField(max_length=50)
    model = models.ForeignKey(DroneModel)
    owner = models.ForeignKey(DroneOwnerProfile, related_name='drones')
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('name',)
