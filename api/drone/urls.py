from django.conf.urls import url

from api.drone.views import DroneView, DroneModelView, DroneModelDetailsView, DroneDetailsView

urlpatterns = [
    url(r'^drones/$', DroneView.as_view(), name='drones'),
    url(r'^drones/(?P<pk>\d+)/$', DroneDetailsView.as_view(), name='drones-details'),
    url(r'^drone-models/$', DroneModelView.as_view(), name='drone-models'),
    url(r'^drone-models/(?P<pk>\d+)/$', DroneModelDetailsView.as_view(), name='drone-models-details'),
]
