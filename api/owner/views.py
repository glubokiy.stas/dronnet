from rest_framework import generics
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated, IsAdminUser

from api.owner.models import DroneOwnerProfile
from api.owner.serializers import DroneOwnerProfileSerializer


class DroneOwnerProfileView(generics.RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = DroneOwnerProfileSerializer
    queryset = DroneOwnerProfile.objects.all()

    def get_object(self):
        return get_object_or_404(self.get_queryset(), user=self.request.user)


class DroneOwnersView(generics.ListAPIView):
    permission_classes = (IsAdminUser,)
    serializer_class = DroneOwnerProfileSerializer
    queryset = DroneOwnerProfile.objects.all()
