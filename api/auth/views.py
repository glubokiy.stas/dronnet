from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework import generics, status
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from api.auth.serializers import UserSerializer
from api.owner.models import DroneOwnerProfile


class SignUpView(generics.GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer

    def post(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            DroneOwnerProfile.objects.create(user=user)
            return Response(data={'token': str(user.auth_token)}, status=status.HTTP_200_OK)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, **kwargs):
    if kwargs.get('created', False):
        Token.objects.create(user=kwargs.get('instance'))
