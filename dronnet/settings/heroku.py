from dronnet.settings import *


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['.dronnet.herokuapp.com']


# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

import dj_database_url
DATABASES['default'].update(dj_database_url.config())
DATABASES['default']['ENGINE'] = 'django.contrib.gis.db.backends.postgis'
